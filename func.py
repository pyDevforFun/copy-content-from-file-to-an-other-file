from sys import argv
from os.path import exists

script, from_file, to_file = argv

print("Copying from %s to %s" % (from_file, to_file))

# i could do these two on one line too, how?
output = open(from_file)
indata = output.read()

print('The input file is %d byte long' % len(indata))
print("Does the output exists? %r" % exists(to_file))
print("Ready, hit RETURN to continue, CTRL-C to abort")
raw_input()

output = open(to_file, 'w')
output.write(indata)

print("ALright, all done.")

output.close()
